What has been refactored:
-
1. Deleted useless files and folders.
2. Deleted gradle from the project, gradle and maven are both build automation tools commonly used in Java projects. They help manage dependencies, compile source code, run tests, and package the project into a distributable format, so we need only one of them - maven as the requirement says.
3. Deleted serenity.conf because currently it stores only web driver props, and we don’t need them at all for the task.
4. Cleared POM file, a bunch of useless dependencies.
5. Steps definition was moved to the main package, because all code that has any logic (it can be utils, helpers, api, pom classes), they are always stored there. Separation of tests and code.
6. Search folder in the test package was removed due to 'Step not found error', I've tried to change the glue property, but it didn't help.
7. Scenario has been rewritten at all, because it wasn't one scenario, it was 3 into one, currently it's scenario outline.

How to use test solution:
-
1. To run java code, you need to have JDK version 8 or higher.
2. If you want to debug, create new tests you should have the intellij idea, at least the community edition with installed the next plugins: cucumber and gherkin.
3. Inside the maven menu do 'clean' and 'install' commands to restore dependencies.
4. You can start the tests by opening feature file and just pressing run icon.
5. You can add a new scenario in the existing feature file or create your own for the new feature.
6. Test report can be downloaded from GitLab > CI/CD > Artifacts > Select a job > Download artifacts.zip
7. Test report is located by path: artifacts.zip > target > site > serenity > index.html