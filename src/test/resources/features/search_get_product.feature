Feature: Search for the product

### Please use endpoint GET https://waarkoop-server.herokuapp.com/api/v1/search/demo/{product} for getting the products.
### Available products: "orange", "apple", "pasta", "cola"
### Prepare Positive and negative scenarios

  Scenario Outline: Get search/demo endpoint should return requested product
    When Sending GET request to search/demo/<product>
    Then Status code of response is 200
    And Response body contains requested <product> product

    Examples:
      | product |
      | orange  |
      | apple   |
      | pasta   |
      | cola    |

  Scenario Outline: Get search/demo should return error for not existing product
    When Sending GET request to search/demo/<product>
    Then Status code of response is 404
    And Response body contains error message Not found

    Examples:
      | product |
      | car     |
      | plane   |
      | man     |

  Scenario: Product price cannot be less or equals to zero
    When Sending GET request to search/demo/orange
    And Product price cannot be less or equals to zero

  Scenario: Product title cannot be empty
    When Sending GET request to search/demo/cola
    And Product title cannot be empty

  Scenario: Not authenticated for Get search/demo endpoint when no product was passed
    When Sending GET request to search/demo/
    Then Status code of response is 401