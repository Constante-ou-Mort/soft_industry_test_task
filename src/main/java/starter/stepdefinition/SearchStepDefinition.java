package starter.stepdefinition;

import io.cucumber.java.Before;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import net.serenitybdd.rest.SerenityRest;
import starter.App;

import java.util.List;

import static net.serenitybdd.rest.SerenityRest.restAssuredThat;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.*;

public class SearchStepDefinition {

    private final String CONTROLLER = "search/demo/";

    @Before
    public void before() {
        SerenityRest.given().baseUri(App.BASE_URL);
    }

    @Then("^Status code of response is (.*)$")
    public void statusCodeOfResponseIs(String expectedStatusCode) {
        restAssuredThat(response -> response.statusCode(Integer.parseInt(expectedStatusCode)));
    }

    @When("^Response body contains requested (.*) product$")
    public void responseBodyContainsRequestedProductProduct(String product) {
        restAssuredThat(response -> {
            List<String> titles = response.extract().jsonPath().getList("title");
            assertThat(titles, hasItem(containsStringIgnoringCase(product)));
        });
    }

    @When("^Sending GET request to search/demo/(.*)$")
    public void sendingGETRequestToSearchDemoProduct(String product) {
        SerenityRest
                .when()
                .get(CONTROLLER + product)
                .body()
                .prettyPrint();
    }

    @Then("^Response body contains error message (.*)$")
    public void responseBodyContainsErrorMessageNotFound(String message) {
        restAssuredThat(response -> response.body("detail.error", equalTo(true)));
        restAssuredThat(response -> response.body("detail.message", equalTo(message)));
    }

    @Then("^Product price cannot be less or equals to zero$")
    public void productsPriceIsGreaterOrEqualsToZero() {
        restAssuredThat(response -> {
            List<Float> prices = response.extract().jsonPath().getList("price");

            assertThat(prices, is(not(empty())));
            for (Float price : prices)
                assertThat(price, greaterThan(0.0F));
        });
    }

    @Then("^Product title cannot be empty$")
    public void productTitleCannotBeEmpty() {
        restAssuredThat(response -> {
            List<String> titles = response.extract().jsonPath().getList("title");

            assertThat(titles, is(not(empty())));
            for (String title : titles) {
                assertThat(title, is(not(emptyString())));
                assertThat(title, is(not(containsStringIgnoringCase("null"))));
            }
        });
    }
}